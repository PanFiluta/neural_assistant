import os
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import NearestNeighbors

DATA_DICT = {
    "actions": {
        "light_on": ["rozsviť", "rozsvítit", "rozsvětli", "rožni"],
        "light_off": ["zhasni", "zhasnout"],
        "turn_on": ["zapni", "zapnout"],
        "turn_off": ["vypni", "vypnout"],
        "info_time": ["hodiny", "hodin", "čas"],
        "info_weather_actual": ["počasí", "stupňů", "teplota", "aktuální", "nyní", "teď", "dnes"],
        "info_weather_forecast": ["počasí", "zítřejší", "předpověď", "předpovězená", "zítra", "budoucí"],
        "info_news": ["zprávy", "novinky"],
        "play_youtube": ["youtube", "přehraj", "zapni tam"],
        "play_radio_kiss": ["rádio kiss", "rádio"],
        "set_volume_main": ["celková hlasitost", "celkovou hlasitost", "hlavní hlasitost", "hlasitost"],
        "verification_one": ["ověřit", "ověř", "ověření"],
        "verification_two": ["identifikuj", "identifikace", "identifikování"],
        "verification_three": ["koho vidíš", "kdo jsem", "vidět"],
        "notify_what_is_going_on": ["co se děje", "co se stalo", "o co jde"],
        "no": ["už nic", "to je všechno", "ne"]
    },
    "places": {
        "kitchen": ["kuchyň", "kuchyni", "kuchyně", "kuchyňa"],
        "bathroom": ["koupelna", "koupelně", "koupelka", "koupelce"],
        "living_room": ["obyvák", "obyváček", "obyvací pokoj"],
        "bedroom": ["ložnice", "ložnička", "spálňa", "spálňu"],
        "dining_room": ["jídelna", "jídelnu"],
        "balcony": ["balkon", "balkonek", "balkón", "balkóně", "balkónu"],
        "garage": ["garáž", "garaži"],
        "basement": ["sklep", "sklepení"],
        "attic": ["půda", "půdu", "půdě", "hůra", "hůru", "hůře"],
        "office": ["kancelář", "kancl", "kancelár", "kanclu"],
        "corridor": ["chodba", "korytar", "koridor"],
        "garden": ["zahrada", "zahrádka", "zahrádku"],
        "terrace": ["terasa", "teras", "terasku"],
        "porch": ["veranda", "verandu", "předsíň", "předsíni"],
        "laundry_room": ["prádelna", "prádelnu", "prádelka", "prádelce", "prádelky"],
        "closet": ["šatna", "skříň", "šatník", "šatně", "šatni"]
    },
    "activations": {
        "agata": ["agáto", "agáta", "agathos", "agátu"],
        "hey_agato": ["hey agáto", "hej agáto"],
    }
}

OUTPUT_DIR = 'TrainOutput'
MODELS = ["model.pkl", "vectorizer.pkl", "labels.pkl"]

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

def train_and_save_model(data, category):
    training_phrases = []
    training_labels = []

    for label, phrases in data.items():
        for phrase in phrases:
            training_phrases.append(phrase)
            training_labels.append(label)

    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(training_phrases)
    model = NearestNeighbors(n_neighbors=1).fit(X)

    with open(os.path.join(OUTPUT_DIR, category + "_model.pkl"), 'wb') as f:
        pickle.dump(model, f)
    with open(os.path.join(OUTPUT_DIR, category + "_vectorizer.pkl"), 'wb') as f:
        pickle.dump(vectorizer, f)
    with open(os.path.join(OUTPUT_DIR, category + "_labels.pkl"), 'wb') as f:
        pickle.dump(training_labels, f)

for category, data in DATA_DICT.items():
    train_and_save_model(data, category)
