import os
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from math import exp

MODEL_DIR = 'src/Predictor/TrainOutput'

# Loading models and vectorizers
models = {}
vectorizers = {}
labels = {}

for category in ["actions", "places", "activations"]:
    with open(os.path.join(MODEL_DIR, category + "_model.pkl"), 'rb') as f:
        models[category] = pickle.load(f)
    with open(os.path.join(MODEL_DIR, category + "_vectorizer.pkl"), 'rb') as f:
        vectorizers[category] = pickle.load(f)
    with open(os.path.join(MODEL_DIR, category + "_labels.pkl"), 'rb') as f:
        labels[category] = pickle.load(f)


def categorize_text(text):
    if not isinstance(text, str) or not text.strip():
        return {'actions': {'label': 'N/A', 'probability': 0.0},
                'places': {'label': 'N/A', 'probability': 0.0},
                'activations': {'label': 'N/A', 'probability': 0.0}}

    results = {}
    for category in ["actions", "places", "activations"]:
        # Text transformation
        X = vectorizers[category].transform([text])
        # Predictions
        distances, indices = models[category].kneighbors(X)
        predicted_label = labels[category][indices[0][0]]

        # Using distance and exponential function to estimate "probability".
        unprobability = distances[0][0]
        probability = exp(-unprobability)
        
        results[category] = {
            'label': predicted_label,
            'probability': probability * 100  # converted to percentage
        }
    return results

'''EXAMPLE
text = "agáto zapni rádio v kuchyni"
print(categorize_text(text))
'''