import re

def play_youtube(keywords):
    keywords = re.sub(r".*(youtube|přehraj|zapni tam)\s*", "", keywords)

    command = keywords
    
    return command

def play_radio(station_name):
    if "kiss" or "rádio" in station_name:
        command = "http://icecast1.play.cz/kiss128.mp3"
    else:
        command = None

    
    return command


def set_volume_main(text):
    match = re.search(r"\b(\d+)\s*%", text)
    if match:
        volume_percentage = match.group(1)
        command = f'amixer sset Master {volume_percentage}%'
        return command
    else:
        return None
    
