from gtts import gTTS
import os

def TextToSpeech(text, voice, lang):
    if voice == "Google":
        tts = gTTS(text=text, lang=lang)
        file_name = "output.mp3"
        tts.save(file_name)
        return file_name
    else:
        print("This voice didn't exist!")
        return None

'''EXAMPLE USAGE
text = "kokote"
file_path = TextToSpeech(text, "Google", "cs")

if file_path:
    os.system(f"mplayer {file_path}")

'''

