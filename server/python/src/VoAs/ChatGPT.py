import json
import openai
from openai.error import AuthenticationError

openai.api_key = "your-api-key-here"
txt_file_path = "src/VoAs/chat_history.txt"

def chatgpt(userName, prompt):
    messages = load_chat_history()
    user_message = f"{userName}: {prompt}"
    messages.append({"role": "user", "content": user_message})
    
    try:
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=messages,
            temperature=1,
            max_tokens=256,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0
        )
        answer = response['choices'][0]['message']['content']
        messages.append({"role": "assistant", "content": f"{answer}"})  
        save_chat_history(messages)
    except AuthenticationError:
        answer = "Chyba ověření: Poskytnutý API klíč je neplatný. Ujistěte se, že jste zadali správný klíč."

    return answer

def load_chat_history():
    try:
        with open(txt_file_path, "r") as file:
            chat_history = file.readlines()
    except FileNotFoundError:
        chat_history = []
    
    messages = []
    for line in chat_history:
        line = line.strip()
        role, message_content = line.split(": ", 1)
        messages.append({"role": role, "content": message_content})
    
    return messages

def save_chat_history(messages):
    with open(txt_file_path, "w") as file:
        for message in messages:
            file.write(f"{message['role']}: {message['content']}\n")

# Test call
#response = chatgpt("David", "Ahoj")
#print(response)
