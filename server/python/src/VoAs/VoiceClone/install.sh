git clone https://github.com/777gt/-EVC-
mv ./-EVC- EVC
curl https://pyenv.run | bash

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv virtualenv-init -)"

pyenv install 3.9.8

pyenv local 3.9.8
python -m venv rvc_venv

source rvc_venv/bin/activate
pip install -r ./-EVC-/requirements.txt
