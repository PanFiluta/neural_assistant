import os
import speech_recognition as sr

def recognize_speech_from_wav(filename):
    if not filename.endswith(".wav"):
        return "Invalid file format"

    # Recognize speech from the audio file
    r = sr.Recognizer()
    with sr.AudioFile(filename) as source:
        audio_data = r.record(source)

        # Adjust for ambient noise
        r.adjust_for_ambient_noise(source)

        try:
            text = r.recognize_google(audio_data, language="cs-CZ")
            return text
        except sr.UnknownValueError:
            return False
        except sr.RequestError as e:
            return f"Error with Google Speech Recognition service: {e}"

if __name__ == "__main__":
    recognized_text = recognize_speech_from_wav("voice.wav")
    print("Recognized text:", recognized_text)

"""----USAGE---IN---MAIN---
from src.VoAs import SpeechRecognition

recognized_text = SpeechRecognition.recognize_speech_from_wav("keyword14.wav")
print("Recognized text:", recognized_text)
"""
