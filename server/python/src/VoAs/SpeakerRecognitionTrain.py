import numpy as np
import matplotlib.pyplot as plt
import os
import librosa
import librosa.display
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout, BatchNormalization
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from sklearn.preprocessing import LabelEncoder
import os
import joblib

print("Načítání a předzpracování dat...")
DATAPATH = "Dataset"
labels = []
mfccs = []

MAX_LENGTH = 1 * 16000 #22050

for speaker in os.listdir(DATAPATH):
    for file in os.listdir(os.path.join(DATAPATH, speaker)):
        if file == ".ipynb_checkpoints":
            continue

        filepath = os.path.join(DATAPATH, speaker, file)
        if not filepath.endswith(('.wav', '.mp3', '.flac')):
            continue

        y, sr = librosa.load(filepath)
        if len(y) > MAX_LENGTH:
            y = y[:MAX_LENGTH]
        else:
            y = np.pad(y, (0, MAX_LENGTH - len(y)), 'constant')

        mfcc = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=20)  # Zvýšený počet MFCC koeficientů
        mfcc = (mfcc - np.mean(mfcc)) / np.std(mfcc)  # Normalizace
        mfcc = np.expand_dims(mfcc, axis=-1)
        labels.append(speaker)
        mfccs.append(mfcc)

X = np.array(mfccs)
y = np.array(labels)

le = LabelEncoder()
y_numeric = le.fit_transform(y)
X_train, X_test, y_train, y_test = train_test_split(X, y_numeric, test_size=0.2, stratify=y_numeric)  # Stratifikované rozdělení

print(f"Celkem zpracovaných souborů: {len(mfccs)}")
print(f"Celkem nalezených mluvčích: {len(np.unique(labels))}")

# Budování modelu
print("Budování modelu...")
model = Sequential()

model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(X_train.shape[1], X_train.shape[2], 1)))
model.add(BatchNormalization())  # Přidání Batch Normalization
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
model.add(BatchNormalization())  # Přidání Batch Normalization
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(len(np.unique(y)), activation='softmax'))

print("Trénink modelu...")
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

early_stop = EarlyStopping(monitor='val_loss', patience=10)
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.001)
history = model.fit(X_train, y_train, epochs=100, validation_data=(X_test, y_test), callbacks=[early_stop, reduce_lr])

print("Ukládání modelu a štítků...")
model.save("TrainOut/model.h5")
joblib.dump(le, "TrainOut/labels.pkl")

loss, accuracy = model.evaluate(X_test, y_test, verbose=0)
print(f"Celková přesnost na testovací sadě: {accuracy*100:.2f}%")

print("Vykreslení přesnosti...")
plt.plot(history.history['accuracy'], label='přesnost tréninku')
plt.plot(history.history['val_accuracy'], label='přesnost testu')
plt.xlabel('Epocha')
plt.ylabel('Přesnost')
plt.legend()
plt.show()

print("Všechny kroky dokončeny!")
