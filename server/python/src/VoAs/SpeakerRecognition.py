from tensorflow.keras.models import load_model
import librosa
import numpy as np
from sklearn.preprocessing import LabelEncoder
import joblib

# Load model and labels
model = load_model("src/VoAs/TrainOut/model.h5")
le = joblib.load("src/VoAs/TrainOut/labels.pkl")


def process_wav_file(file_path, max_length=1 * 16000):
    y, sr = librosa.load(file_path)

    # Trimming or adding a recording to MAX_LENGTH
    if len(y) > max_length:
        y = y[:max_length]
    else:
        y = np.pad(y, (0, max_length - len(y)), 'constant')

    mfcc = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=20)  # Změna na 20 MFCC koeficientů
    mfcc = (mfcc - np.mean(mfcc)) / np.std(mfcc)  # Normalizace
    mfcc = np.expand_dims(mfcc, axis=-1)

    # The output form is (1, feature_dim_1, feature_dim_2, channel) for model.predict
    return np.array([mfcc])

def predict_speaker_from_wav(file_path):
    input_data = process_wav_file(file_path)
    predictions = model.predict(input_data)

    predicted_index = np.argmax(predictions)
    predicted_speaker = le.classes_[predicted_index]
    
    results = []
    results.append(f"Nejpravděpodobnější mluvčí: {predicted_speaker}")
    results.append("Pravděpodobnosti pro jednotlivé mluvčí:")
    for i, speaker in enumerate(le.classes_):
        results.append(f"{speaker}: {predictions[0][i]:.4f}")
        
    #return "\n".join(results)
    return predicted_speaker

if __name__ == "__main__":
    file_path = "test.mp3"
    print(predict_speaker_from_wav(file_path))

"""----USAGE---IN---MAIN---
from src.VoAs import SpeakerRecognition
print(SpeakerRecognition.predict_speaker_from_wav("keyword14.wav"))
"""
