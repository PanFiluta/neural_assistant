import os
import wave
import pyaudio
import numpy as np

RATE = 16000
MAX_CLIPS = 200
FOLDER_NAME = "background_noise"
SECONDS = 1
CHUNK_SIZE = 1024
CHANNELS = 1
SILENCE_THRESHOLD = 500  
CHECK_DURATION = 0.2  

if not os.path.exists(FOLDER_NAME):
    os.makedirs(FOLDER_NAME)

p = pyaudio.PyAudio()

stream = p.open(format=pyaudio.paInt16,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK_SIZE)

print("Start mluvení...")

i = 0
while i < MAX_CLIPS:
    frames = []

    for _ in range(0, int(RATE / CHUNK_SIZE * CHECK_DURATION)):
        data = stream.read(CHUNK_SIZE)
        frames.append(data)
    
    waveform = np.frombuffer(b"".join(frames), dtype=np.int16)
    
    if np.max(np.abs(waveform)) > SILENCE_THRESHOLD:
        for _ in range(int(RATE / CHUNK_SIZE * CHECK_DURATION), int(RATE / CHUNK_SIZE * SECONDS)):
            data = stream.read(CHUNK_SIZE)
            frames.append(data)
        
        file_name = os.path.join(FOLDER_NAME, f"clip_{i}.wav")
        with wave.open(file_name, 'wb') as wf:
            wf.setnchannels(CHANNELS)
            wf.setsampwidth(p.get_sample_size(pyaudio.paInt16))
            wf.setframerate(RATE)
            wf.writeframes(b"".join(frames))
        print(f"Nahráno: {file_name}")
        i += 1
    else:
        print(f"Zjištěn tichý začátek. Zkouším znovu...")

stream.stop_stream()
stream.close()
p.terminate()
