import requests
from datetime import datetime
import feedparser
import re
import time

def get_location():
    # Obtaining a public IP address
    response = requests.get('https://api.ipify.org?format=json')
    ip_data = response.json()
    ip_address = ip_data['ip']

    # Obtaining geolocation data based on IP address using GeolocationDB
    response = requests.get(f'https://geolocation-db.com/json/{ip_address}&position=true')
    if response.status_code != 200:
        raise ValueError(f"API responded with status {response.status_code}: {response.text}")
    location_data = response.json()

    # Getting coordinates from geolocation data
    latitude = location_data['latitude']
    longitude = location_data['longitude']
    city = location_data['city']

    return latitude, longitude, city


def get_wind_direction(angle):
    # Translation of wind angle to text description of direction
    if 22.5 <= angle < 67.5:
        return 'severovýchod'
    elif 67.5 <= angle < 112.5:
        return 'východ'
    elif 112.5 <= angle < 157.5:
        return 'jihovýchod'
    elif 157.5 <= angle < 202.5:
        return 'jih'
    elif 202.5 <= angle < 247.5:
        return 'jihozápad'
    elif 247.5 <= angle < 292.5:
        return 'západ'
    elif 292.5 <= angle < 337.5:
        return 'severozápad'
    else:
        return 'sever'

def get_weather_description(weather_code):
    weather_descriptions = {
        0: "jasno",
        1: "skoro jasno",
        2: "částečně oblačno",
        3: "oblačno",
        45: "mlha a námraza",
        48: "námraza",
        51: "mrholení: lehká intenzita",
        53: "mrholení: střední intenzita",
        55: "mrholení: silná intenzita",
        56: "mrznoucí mrholení: lehká intenzita",
        57: "mrznoucí mrholení: silná intenzita",
        61: "déšť: slabá intenzita",
        63: "déšť: střední intenzita",
        65: "déšť: silná intenzita",
        66: "mrznoucí déšť: lehká intenzita",
        67: "mrznoucí déšť: silná intenzita",
        71: "sněžení: slabá intenzita",
        73: "sněžení: střední intenzita",
        75: "sněžení: silná intenzita",
        77: "sněhové zrnky",
        80: "přeháňky deště: slabá intenzita",
        81: "přeháňky deště: střední intenzita",
        82: "přeháňky deště: silná intenzita",
        85: "přeháňky sněhu: slabá intenzita",
        86: "přeháňky sněhu: silná intenzita",
        95: "bouřka: slabá nebo střední intenzita",
        96: "bouřka s malými krupobitími: slabá intenzita",
        99: "bouřka s malými krupobitími: silná intenzita"
    }

    return weather_descriptions.get(weather_code, "Neznámý kód počasí")

def get_time():
    now = datetime.now()
    output = "je " + now.strftime("%H:%M")
    return output

def get_weather_actual():
    # Getting the location
    latitude, longitude, city = get_location()

    # Getting weather using OpenMeteo API
    response = requests.get(f'https://api.open-meteo.com/v1/forecast?latitude={latitude}&longitude={longitude}&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m')
    data = response.json()

    # Current weather
    current_weather = data['current_weather']
    current_time = datetime.now()
    temperature = current_weather['temperature']
    weather_code = current_weather['weathercode']
    windspeed = current_weather['windspeed']
    winddirection = current_weather['winddirection']

    # Weather presenter style current weather output
    output = f"Aktuální počasí {city}:\n"
    if 6 <= current_time.hour < 10:
        output += f"Dobré ráno, je {current_time.strftime('%H:%M')}.\n"
    elif 10 <= current_time.hour < 12:
        output += f"Dobré dopoledne, je {current_time.strftime('%H:%M')}.\n"
    elif 12 <= current_time.hour < 18:
        output += f"Dobré odpoledne, je {current_time.strftime('%H:%M')}.\n"
    else:
        output += f"Dobrý večer, je {current_time.strftime('%H:%M')}.\n"

    output += f"Venku je teplota {temperature} stupňů Celsia.\n"
    output += f"Očekáváme počasí {get_weather_description(weather_code)}.\n"
    output += f"Vítr fouká rychlostí {windspeed} kilometrů za hodinu směrem na {get_wind_direction(winddirection)}.\n"

    # Hourly forecast
    hourly_data = data['hourly']
    hourly_time = hourly_data['time']
    temperature_2m = hourly_data['temperature_2m']
    relative_humidity_2m = hourly_data['relativehumidity_2m']
    windspeed_10m = hourly_data['windspeed_10m']

    # Indices for the forecast of the next two hours
    current_hour_index = current_time.hour
    next_two_hours = [current_hour_index + 1, current_hour_index + 2]

    # Weather presenter style hourly forecast output
    output += "\nHodinová předpověď:\n"
    output += "Předpovídané počasí pro příští dvě hodiny.\n"
    for index in next_two_hours:
        time = datetime.strptime(hourly_time[index], "%Y-%m-%dT%H:%M")
        time_string = time.strftime("%H:%M")
        output += f"{time_string}: Teplota {temperature_2m[index]} stupňů Celsia, vlhkost {relative_humidity_2m[index]}%, rychlost větru {windspeed_10m[index]} km/h.\n"

    return output

def get_weather_forecast():
    # Getting the location
    latitude, longitude, city = get_location()

    # Getting weather using OpenMeteo API
    response = requests.get(f'https://api.open-meteo.com/v1/forecast?latitude={latitude}&longitude={longitude}&hourly=temperature_2m,weathercode&forecast_days=3')
    data = response.json()

    # Getting the index to start the next day
    current_time = datetime.now()
    tomorrow_start_index = 0
    for index, time in enumerate(data['hourly']['time']):
        forecast_time = datetime.strptime(time, "%Y-%m-%dT%H:%M")
        if forecast_time.day != current_time.day:
            tomorrow_start_index = index
            break

    # Weather for the next day
    tomorrow_time = data['hourly']['time'][tomorrow_start_index:tomorrow_start_index+3]
    temperature_tomorrow = data['hourly']['temperature_2m'][tomorrow_start_index:tomorrow_start_index+3]
    weather_code_tomorrow = data['hourly']['weathercode'][tomorrow_start_index:tomorrow_start_index+3]

    # Next day weather forecast output
    output = f"Předpověď počasí pro zítřek {city}:\n"
    output += f"Ráno: {get_weather_description(weather_code_tomorrow[0])}, teplota {temperature_tomorrow[0]} °C.\n"
    output += f"Odpoledne: {get_weather_description(weather_code_tomorrow[1])}, teplota {temperature_tomorrow[1]} °C.\n"
    output += f"Večer: {get_weather_description(weather_code_tomorrow[2])}, teplota {temperature_tomorrow[2]} °C.\n"

    return output


def get_latest_news(num_articles):
    NewsFeed = feedparser.parse("https://www.ceskenoviny.cz/sluzby/rss/zpravy.php")
    entries = NewsFeed.entries[:num_articles]
    
    result = ""
    for index, entry in enumerate(entries, start=1):
        result += "Zpráva {}: ".format(index) + entry.title + "\n"
        #result += "Shrnutí: " + entry.summary + "\n\n"
    
    return result


