import os
from datetime import datetime, timedelta
from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
from src.VoAs import SpeakerRecognition, SpeakerRecognition
from src.VoAs import SpeechRecognition, VoiceSynthesis, ChatGPT
from src.Predictor import Predictor
from src.NotifyBus import Info
from src.Player import Play
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
app = Flask("Server")
CORS(app)

SPEAKER_CONFIG = {
    "dominik_campa": {
        "greeting": "Dominiku! ",
        "end_greeting": "... je to všechno , co pro tebe mohu udělat?"
    }
}

VERIFIED_ACTIONS = ["verification_three"]



class UserSession:
    def __init__(self):
        self.active = False
        self.last_interaction = None

    def activate(self):
        self.active = True
        self.last_interaction = datetime.now()
        logging.debug("User session activated.")

    def deactivate(self):
        self.active = False
        self.last_interaction = None
        logging.debug("User session deactivated.")

    def update_interaction(self):
        self.last_interaction = datetime.now()
        logging.debug("User session interaction updated.")

    def is_active(self):
        if not self.active:
            return False
        if datetime.now() - self.last_interaction > timedelta(seconds=20):
            self.deactivate()
        return self.active

sessions = {}

sessions = {"0": UserSession()}
def recognize(file_path):
    global sessions
    # Recognize speech and speaker
    recognized_text = SpeechRecognition.recognize_speech_from_wav(file_path)
    recognized_speaker = SpeakerRecognition.predict_speaker_from_wav(file_path)
    predictor_output = Predictor.categorize_text(recognized_text)

    # Extract the relevant information from the predictor output
    activation_probability = predictor_output['activations']['probability']
    action_label = predictor_output['actions']['label']
    action_probability = predictor_output['actions']['probability']

    # Print for debugging purposes
    print(action_label)
    print(action_probability)

    # Construct a new filename using speaker's name and timestamp
    timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
    new_filename = f"{timestamp}_{recognized_speaker}.wav"
    new_save_path = os.path.join("ReceivedVoices", new_filename)

    # Rename (or move) the file
    os.rename(file_path, new_save_path)

    is_verified_speaker = recognized_speaker in SPEAKER_CONFIG

    if activation_probability == 100.0:
        sessions["0"].activate()
    elif sessions["0"].is_active():
        sessions["0"].update_interaction()
    else:
        # Note: This is where you're checking if the activation word isn't found.
        # You might also want to handle the case when activation_probability is, let's say, greater than 80% or 90% but not 100%.
        if activation_probability != 100.0:
            print("Activation word not found")

    if recognized_speaker in SPEAKER_CONFIG:
        greeting_message = SPEAKER_CONFIG[recognized_speaker]["greeting"] + " "
    else:
        greeting_message = ""

    ResponseCommand = None
    if sessions["0"].is_active():
        # Pokud je akce v seznamu VERIFIED_ACTIONS a recognized_speaker není v SPEAKER_CONFIG, vrátíme chybovou zprávu.
        if action_label in VERIFIED_ACTIONS and not is_verified_speaker:
            synthesized_path = VoiceSynthesis.TextToSpeech("Nerozpoznaný mluvčí, verifikace selhala.", "Google", "cs")
        else:
            # Pokud je akce verifikovaná a mluvčí je ověřený, nebo pokud akce nevyžaduje verifikaci, provádíme danou akci.
            if action_label == "info_time":
                InfoOutput = Info.get_time()
                synthesized_path = VoiceSynthesis.TextToSpeech(greeting_message + InfoOutput, "Google", "cs")
                sessions["0"].deactivate() 
            # Přidání vaší nové akce
            elif action_label == "info_weather_actual":
                InfoOutput = Info.get_weather_actual()
                synthesized_path = VoiceSynthesis.TextToSpeech(greeting_message + InfoOutput, "Google", "cs")
                sessions["0"].deactivate() 

            elif action_label == "info_weather_forecast":
                InfoOutput = Info.get_weather_forecast()
                synthesized_path = VoiceSynthesis.TextToSpeech(greeting_message + InfoOutput, "Google", "cs")
                sessions["0"].deactivate() 

            elif action_label == "info_news":
                InfoOutput = Info.get_latest_news(3)
                synthesized_path = VoiceSynthesis.TextToSpeech(greeting_message + InfoOutput, "Google", "cs")
                sessions["0"].deactivate() 

            elif action_label == "play_youtube":
                ResponseCommand = Play.play_youtube(recognized_text)
                synthesized_path = VoiceSynthesis.TextToSpeech(greeting_message + "Hned to bude", "Google", "cs")
                sessions["0"].deactivate() 

            elif action_label == "play_radio_kiss":
                ResponseCommand = Play.play_radio(recognized_text)
                synthesized_path = VoiceSynthesis.TextToSpeech(greeting_message + "Užij si poslech!", "Google", "cs")
                sessions["0"].deactivate() 

            elif action_label == "verification_one" or action_label == "verification_two" or action_label == "verification_three":
                synthesized_path = VoiceSynthesis.TextToSpeech("Verifikace proběhla úspěšně, uživatel:" + recognized_speaker, "Google", "cs")

            elif action_label == "turn_off":
                ResponseCommand = "stop"
                synthesized_path = VoiceSynthesis.TextToSpeech("Dobře , " + greeting_message + " Přeješ si ještě něco?", "Google", "cs")

            elif action_label == "no":
                synthesized_path = VoiceSynthesis.TextToSpeech("Dobře, " + greeting_message + " Kdyby něco, stačí říct a já se přivanu, jako vítr!", "Google", "cs")
                sessions["0"].deactivate() 

            else:
                synthesized_path = VoiceSynthesis.TextToSpeech(greeting_message + " Omlouvám se, ale tato funkce není dostupná!", "Google", "cs")
                sessions["0"].deactivate() 
    else:
        synthesized_path = ""
    # Return a response dictionary
    response = {
        "recognized_text": recognized_text,
        "recognized_speaker": recognized_speaker,
        "predictor_output": predictor_output,
        "synthesized_voice_path": synthesized_path,
        "ResponseCommand": ResponseCommand
    }

    return response


@app.route('/Recognize', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify(error="No file part"), 400

    file = request.files['file']

    if file.filename == '':
        return jsonify(error="No selected file"), 400

    if file:
        if not os.path.exists("ReceivedVoices"):
            os.makedirs("ReceivedVoices")
        
        # Get the original filename and extension
        original_filename = file.filename
        original_extension = original_filename.rsplit('.', 1)[1].lower()

        # Save the file using the original filename and extension
        temp_save_path = os.path.join("ReceivedVoices", original_filename)
        file.save(temp_save_path)

        # Call recognize() function to process the uploaded file
        response = recognize(temp_save_path)
        
        return jsonify(response)

    return jsonify(error="Unexpected error"), 500



@app.route('/get_voice/<filename>', methods=['GET'])
def get_voice(filename):
    return send_from_directory('', filename)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
