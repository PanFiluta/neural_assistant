import speech_recognition as sr
import threading
from pydub import AudioSegment
import requests
import os
import json
import subprocess

process_player = subprocess.Popen(['python3', 'src/Player.py'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
global SERVER_IP
SERVER_IP = "0.0.0.0"

def send_command(command):
    url = "http://0.0.0.0:9999/play"
    data = {"filename": command}
    output = None
    try:
        response = requests.post(url, data=data)
        output = response.text
    except:
        output = "Failed to play!"

    return output

def set_volume(volume):
    url = "http://0.0.0.0:9999/volume"
    data = {"volume": volume}

    try:
        response = requests.post(url, data=data)
        output = response.text
    except:
        output = "Failed to play!"

    return output

def send_stop():
    url = "http://0.0.0.0:9999/stop"
    
    try:
        response = requests.get(url)
        output = response.text
    except Exception as e:
        output = f"Failed to stop! Error: {e}"

    return output

def get_player_status():
    url = "http://0.0.0.0:9999/player_info"
    try:
        response = requests.get(url)
        if response.status_code == 200:
            player_info = response.json()
            return player_info.get("status", "Stopped")
        else:
            return "Stopped"
    except requests.exceptions.RequestException:
        return "Stopped"

def calibrate_ambient_noise(source, recognizer):
    recognizer.adjust_for_ambient_noise(source)
    energy_threshold = recognizer.energy_threshold

    # Calculate the percentage of calibration
    percentage = int(((energy_threshold - 50) / (4000 - 50)) * 100)
    filled_blocks = int((percentage / 10) + 0.5)  # Round the value
    empty_blocks = 10 - filled_blocks

    progress_bar = "[" + "#" * filled_blocks + "-" * empty_blocks + "]"
    print("Current ambient noise calibration value: %s", progress_bar)

def save_audio(audio, file_name):
    audio_segment = AudioSegment.from_wav(file_name)
    audio_segment.export(file_name, format="wav")


def record_and_send():
    Phrase_time_limit = 15
    r = sr.Recognizer()
    SERVER_URL = "http://" + SERVER_IP + ":5000/Recognize"
    DOWNLOAD_URL = "http://" + SERVER_IP + ":5000/get_voice/"
    
    with sr.Microphone(sample_rate=16000) as source:
        while True:
            calibrate_ambient_noise(source, r)

            player_status = get_player_status()
            timeout_value = 3 if player_status == "Playing" else None
            print(f"timeout_value:{timeout_value}")
            try:
                # Phrase_time_limit added to ensure it doesn't record indefinitely
                audio = r.listen(source, timeout=timeout_value, phrase_time_limit = Phrase_time_limit)
            except sr.WaitTimeoutError:
                print(f"Didn't detect any speech for {Phrase_time_limit} seconds. Listening again...")
                continue


            file_name = "voice.wav"
            with open(file_name, "wb") as f:
                f.write(audio.get_wav_data())

            try:
                text = r.recognize_google(audio, language="cs-CZ")
                print("Recognized voice:", text)
            except sr.UnknownValueError:
                print("Speech not recognized")
            except sr.RequestError as e:
                print("Error communicating with Google Speech Recognition service:", e)

            # Send audio to server
            try:
                with open(file_name, 'rb') as f:
                    response = requests.post(SERVER_URL, files={'file': f})
                if response.status_code == 200:
                    print("File uploaded successfully.")
                    response_data = response.json()
                    print(response_data)

                    # Extract synthesized voice file path from the response
                    synthesized_voice_path = response_data.get("synthesized_voice_path")
                    ResponseCommand = response_data.get("ResponseCommand")
                    if ResponseCommand != "stop":
                        send_command(ResponseCommand)
                    else:
                        send_stop()
                    if synthesized_voice_path:
                        # Download the synthesized voice
                        voice_response = requests.get(DOWNLOAD_URL + synthesized_voice_path, stream=True)
                        with open("synthesized_voice.wav", "wb") as out_file:
                            for chunk in voice_response.iter_content(chunk_size=1024):
                                out_file.write(chunk)

                        # Play the synthesized voice using mplayer
                        VolumeControl = set_volume(50)
                        os.system("mpg123 -q sounds/start.mp3 synthesized_voice.wav")
                        VolumeControl = set_volume(100)

                else:
                    print("Failed to upload the file to server.")
            except requests.exceptions.RequestException as e:
                os.system("mpg123 -q sounds/start.mp3 sounds/internet_failure.mp3")
                print("Error sending the request to the server:", e)
            




if __name__ == "__main__":
    thread1 = threading.Thread(target=record_and_send)
    thread1.start()