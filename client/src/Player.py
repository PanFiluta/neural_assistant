from flask import Flask, render_template, request
import mpv
from flask_cors import CORS
import subprocess
from youtubesearchpython import VideosSearch

app = Flask(__name__)
CORS(app)

player = mpv.MPV(log_handler=print)
video_title = ''  # Initialize the global variable

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

@app.route('/assets/js/player.js')
def serve_player_js():
    return render_template('assets/js/player.js')

@app.route('/assets/js/refresh.js')
def serve_refresh_js():
    return render_template('assets/js/refresh.js')

@app.route('/assets/js/shelly.js')
def serve_shelly_js():
    return render_template('assets/js/shelly.js')

@app.route('/assets/js/weather.js')
def serve_weather_js():
    return render_template('assets/js/weather.js')

@app.route('/')
def index():
    return render_template('player.html', current_file='', current_time='--:--', duration='--:--', status='')

def format_time(seconds):
    if seconds is None:
        return '--:--'
    minutes, seconds = divmod(int(seconds), 60)
    return f"{minutes:02d}:{seconds:02d}"

@app.route('/play', methods=['POST'])
def play():
    global player, video_title
    filename = request.form['filename']
    player.stop() #stop player if playing
    video_title = '' #reset video_title

    if filename.endswith(('.mp3', '.ogg', '.flac', '.wav', '.m4a')):
        player.play(filename)
        return render_template('player.html', current_file=filename, current_time=format_time(player.time_pos), duration=format_time(player.duration) if player.duration else '--:--', status='Playing')
    else:
        print("přehrávám z youtube")
        videosSearch = VideosSearch(filename, limit=1)
        results = videosSearch.result()
        if results["result"]:
            print(results)
            video_id = results["result"][0]['id']
            video_title = results["result"][0]['title']
            video_url = f'https://www.youtube.com/watch?v={video_id}'
            player = mpv.MPV(ytdl=True, video='no')
            player.play(video_url)
            return render_template('player.html', current_file=video_url, current_time=format_time(player.time_pos), duration=format_time(player.duration) if player.duration else '--:--', status='Playing')
        else:
            print(f"No results found for '{filename}' on YouTube.")
            return render_template('player.html', current_file='', current_time='--:--', duration='--:--', status='Stopped')

@app.route('/pause')
def pause():
    global player
    player.pause = not player.pause
    return render_template('player.html', current_file=request.form.get('filename', ''), current_time=format_time(player.time_pos), duration=format_time(player.duration) if player.duration else '--:--', status='Paused' if player.pause else 'Playing')

@app.route('/stop')
def stop():
    global player, video_title
    player.stop()
    video_title = ''  # Reset the video title
    return render_template('player.html', current_file='', current_time='--:--', duration='--:--', status='Stopped')


@app.route('/player_info')
def player_info():
    global player, video_title
    current_time = format_time(player.time_pos)
    duration = format_time(player.duration) if player.duration else '--:--'
    
    # Check if the player is still playing
    if player.playback_abort:
        status = 'Stopped'
        video_title = ''  # Reset the video title
    else:
        status = 'Paused' if player.pause else 'Playing'
    
    # Check if player.metadata exists and retrieve 'icy-title'
    if player.metadata is not None and video_title == '':
        icy_title = player.metadata.get('icy-title', '')
    else:
        icy_title = video_title

    return {
        "current_time": current_time,
        "duration": duration,
        "status": status,
        "info": {
            "display-tags": {
                "icy-title": icy_title,
            }
        }
    }

@app.route('/volume', methods=['POST'])
def set_volume():
    global player
    volume = request.form.get('volume', type=float)  # Get volume from form data, as a float

    if volume is not None:
        player.volume = volume  # Set volume in the range [0.0, 100.0] where 100 is the max volume
        return {'status': 'success', 'message': f'Volume set to {volume}'}
    else:
        return {'status': 'error', 'message': 'Volume not provided or invalid'}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9999, debug=True)
