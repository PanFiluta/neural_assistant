function getCurrentTime() {
    const now = new Date();
    const hours = now.getHours().toString().padStart(2, '0');
    const minutes = now.getMinutes().toString().padStart(2, '0');
    const seconds = now.getSeconds().toString().padStart(2, '0');
    return `${hours}:${minutes}:${seconds}`;
  }

  async function updateWeather() {
  try {
    const response = await fetch(
      'https://api.open-meteo.com/v1/forecast?latitude=49&longitude=18&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m'
    );
    if (!response.ok) {
      throw new Error('Weather data not available');
    }
    const data = await response.json();
    const weatherCode = data.current_weather.weathercode;
    const temperature = data.current_weather.temperature.toFixed(1);
    const weatherIcons = {
      0: 'fas fa-sun',                     // Clear sky
      1: 'fas fa-cloud-sun',               // Few clouds
      2: 'fas fa-cloud',                   // Partly cloudy
      3: 'fas fa-cloud',                   // Partly cloudy
      45: 'fas fa-smog',                   // Fog
      48: 'fas fa-frost',                  // Frost
      51: 'fas fa-cloud-showers-heavy',    // Light rain showers
      53: 'fas fa-cloud-showers-heavy',    // Light rain showers
      55: 'fas fa-cloud-showers-heavy',    // Light rain showers
      56: 'fas fa-snowflake',              // Light snow showers
      57: 'fas fa-snowflake',              // Light snow showers
      61: 'fas fa-cloud-rain',             // Rain
      63: 'fas fa-cloud-rain',             // Rain
      65: 'fas fa-cloud-rain',             // Rain
      66: 'fas fa-cloud-meatball',         // Sleet
      67: 'fas fa-cloud-meatball',         // Sleet
      71: 'fas fa-snowflake',              // Snow showers
      73: 'fas fa-snowflake',              // Snow showers
      75: 'fas fa-snowflake',              // Snow showers
      77: 'fas fa-snowflake',              // Snow showers
      80: 'fas fa-cloud-rain',             // Rain showers
      81: 'fas fa-cloud-rain',             // Rain showers
      82: 'fas fa-cloud-rain',             // Rain showers
      85: 'fas fa-cloud-showers-heavy',    // Heavy rain showers
      86: 'fas fa-cloud-showers-heavy',    // Heavy rain showers
      95: 'fas fa-poo-storm',              // Thunderstorm
      96: 'fas fa-poo-storm',              // Thunderstorm
      99: 'fas fa-poo-storm',              // Thunderstorm
    };
    const weatherIcon = weatherIcons[weatherCode] || 'fas fa-question';

    const weatherDescriptions = {
      61: 'slabý déšť',        // Light rain
      63: 'déšť',             // Rain
      65: 'silný déšť',        // Heavy rain
      66: 'déšť se sněhem',    // Sleet
      71: 'slabé sněžení',     // Snow showers
      73: 'sněžení',           // Snow showers
      75: 'silné sněžení',     // Snow showers
      77: 'vítr a sníh',      // Snow showers
      80: 'slabé přeháňky',   // Rain showers
      81: 'přeháňky',         // Rain showers
      82: 'silné přeháňky',   // Rain showers
      85: 'slabé bouřky',     // Heavy rain showers
      86: 'bouřky',           // Heavy rain showers
      95: 'silné bouřky',     // Thunderstorm
      96: 'bouřky a déšť',    // Thunderstorm
      99: 'silné bouřky',     // Thunderstorm
    };

    const weatherDescription = weatherDescriptions[weatherCode] || '';

    document.getElementById('weather').innerHTML = `
      <i class="${weatherIcon}"></i> ${temperature}°C<br>
      ${weatherDescription}
    `;
  } catch (error) {
    console.error('Error fetching weather data:', error.message);
    document.getElementById('weather').textContent = 'N/A';
  }
}

// Function to update the time every second
function updateTime() {
  document.getElementById('time').textContent = getCurrentTime();
}

// Function to update the weather every 15 minutes
async function updateWeatherPeriodically() {
  updateWeather();
  setInterval(updateWeather, 900000); // 15 minutes in milliseconds
}

// Call the functions to update the time and weather
updateTime(); // Initially update the time
setInterval(updateTime, 1000); // Update the time every 1 second
updateWeatherPeriodically(); // Initially update the weather and set the interval for periodic updates
