// Function to refresh the page
function refreshPage() {
    location.reload();
  }
  
  // Assigning the function to the button after the page loads
  window.addEventListener('load', function () {
    const refreshButton = document.getElementById('refreshButton');
    refreshButton.addEventListener('click', refreshPage);
  });
  