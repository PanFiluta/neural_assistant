let server_ip;
let clientId;
let clientPlace;

document.addEventListener("DOMContentLoaded", async () => {
  try {
    const response = await fetch("../client.json");
    const clientData = await response.json();
    server_ip = clientData.server_ip; 
    clientId = clientData.client_id;
    const url = `http://${server_ip}:3000/client/check/${clientId}`;

    const fetchData = await fetch(url);
    const resultData = await fetchData.json();

    clientPlace = resultData.client_data.client_place;
    console.log(clientPlace);
  } catch (error) {
    console.error("An error occurred:", error);
  }
});

// Function to update the content of a Shelly tile within the specified container
function updateShellyTileContent(container, shellyItem) {
  const tile = document.createElement('div');
  tile.classList.add('empty-tile');

  const tileBody = document.createElement('div');

  const title = document.createElement('h1');
  title.classList.add('card-title');
  title.textContent = shellyItem.name;

  const icon = document.createElement('i');
  icon.classList.add('fas', 'fa-power-off', 'btn-icon');

  const button = document.createElement('button');
  button.appendChild(icon);
  button.classList.add('btn');

  if (shellyItem.relay0 === 1) {
    button.classList.add('btn-warning');
  } else {
    button.classList.add('btn-info');
  }

  button.addEventListener('click', async () => {
    try {
      const response = await fetch(`http://${server_ip}:3000/shelly/toggle/${shellyItem.ip}`, {
        method: 'POST',
      });
      const data = await response.json();
    } catch (error) {
      console.error('Error toggling relay:', error);
    }
  });

  //tileBody.appendChild(title);
  tileBody.appendChild(button);
  tile.appendChild(tileBody);

  container.appendChild(tile);
}

async function fetchAndUpdateShellyTiles() {
  try {
    const response = await fetch('http://' + server_ip + ':3000/shelly/list');
    const data = await response.json();
    console.log(clientPlace);
    
    // Filter the Shelly items with "showed" equal to 1 and name equal to clientPlace
    const showedShellyItems = data.shelly_data.filter(item => item.showed === 1 && item.name === clientPlace);

    let shellyTilesContainer = document.getElementById('light');
    shellyTilesContainer.innerHTML = '';

    showedShellyItems.forEach(item => {
      updateShellyTileContent(shellyTilesContainer, item);
    });
  } catch (error) {
    console.error('Error fetching Shelly data:', error);
  }
}

fetchAndUpdateShellyTiles();
setInterval(fetchAndUpdateShellyTiles, 1000);