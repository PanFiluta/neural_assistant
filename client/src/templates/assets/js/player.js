// player.js (assets/js/player.js)
function fetchPlayerInfo() {
  fetch('/player_info')
    .then(response => response.json())
    .then(data => {
      const currentTimeElement = document.getElementById('current_time');
      const durationElement = document.getElementById('duration');
      const playPauseButton = document.getElementById('playPauseButton');
      const stopButton = document.getElementById('stopButton');
      const songTitleElement = document.getElementById('songTitle');

      if (data.info['display-tags']['icy-title']) {
        // The title of the song is available, only the title is displayed
        const songTitle = data.info['display-tags']['icy-title'];
        songTitleElement.textContent = songTitle;
        songTitleElement.style.textTransform = 'uppercase'; // Set lowercase style for song title
        songTitleElement.style.display = 'block';
        currentTimeElement.style.display = 'none';
        durationElement.style.display = 'none';
        songTitleElement.classList.add('scrolling-text');

        // Check if the song title width exceeds its container width
        if (songTitleElement.scrollWidth > songTitleElement.offsetWidth) {
          // If it does, add a CSS class to enable scrolling animation
          songTitleElement.classList.add('scrolling-text');
        } else {
          // If the title fits within the container, remove the scrolling class
          songTitleElement.classList.remove('scrolling-text');
        }
      } else {
        // Track name is not available, show time and length
        currentTimeElement.textContent = `${data.current_time}-`;
        durationElement.textContent = `${data.duration}`;
        songTitleElement.style.display = 'none';
        currentTimeElement.style.display = 'block';
        durationElement.style.display = 'block';
      }

      if (data.status === 'Stopped') {
        // Playback is stopped, hide play/pause button
        playPauseButton.style.display = 'none';
        stopButton.style.display = 'none';
        currentTimeElement.textContent = `nic se nepřehrává`;
        durationElement.textContent = ``;
      } else {
        // Playback is not stopped, display play/pause button
        playPauseButton.style.display = 'block';
        stopButton.style.display = 'block';
      }

      if (data.status === 'Playing') {
        playPauseButton.innerHTML = '<i class="fas fa-pause"></i>';
      } else if (data.status === 'Paused') {
        playPauseButton.innerHTML = '<i class="fas fa-play"></i>';
      }
      stopButton.innerHTML = '<i class="fas fa-stop"></i>';
    })
    .catch(error => console.error('Error fetching player info:', error));
}

function playPause() {
  fetch('/pause', { method: 'GET' })
    .then(response => {
      fetchPlayerInfo();
    })
    .catch(error => console.error('Error while trying to play/pause:', error));
}

function stop() {
  fetch('/stop', { method: 'GET' })
    .then(response => {
      fetchPlayerInfo();
    })
    .catch(error => console.error('Error while trying to stop:', error));
}


const playPauseButton = document.getElementById('playPauseButton');
playPauseButton.addEventListener('click', playPause);


const stopButton = document.getElementById('stopButton');
stopButton.addEventListener('click', stop);


fetchPlayerInfo();
setInterval(fetchPlayerInfo, 1000);
