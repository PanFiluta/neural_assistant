#!/bin/bash
# Check if the script is being run without root privileges or sudo
#apt-get install libsndfile1
if [ "$EUID" -eq 0 ]; then
    echo "This script cannot be run as root or using sudo."
    exit 1
fi

python3 -m venv client_venv
source client_venv/bin/activate

pip install -r requirements.txt
deactivate
